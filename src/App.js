import React, {Fragment} from 'react';
import './App.scss';
import Header from "./components/Header";
import Archive from "./components/products/Archive";
import Single from "./components/products/Single";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
    return (
        <Fragment>
            <Router>
                <Header/>
                <div className='container'>
                    <Switch>
                        <Route exact path='/' component={Archive}/>
                        <Route path='/product/:id' component={Single}/>
                    </Switch>
                </div>
            </Router>
        </Fragment>
    );
}

export default App;
