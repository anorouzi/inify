import React, {useEffect, useState} from "react";
import axios from 'axios';
import Loading from "./Loading";
import namad from '../assets/imgs/nemad.png';

const Seller = () => {
    const [sellerInfo, setSellerInfo] = useState([]);

    useEffect(() => {
        axios.get(`seller`)
            .then(res => {
                setSellerInfo(res.data);
            })
    }, []);

    if (sellerInfo.businessInfo === undefined) {
        return <div className='center'><Loading/></div>
    }

    const {businessName, businessStatus, fullName, profilePicUrl, biography, shopRating} = sellerInfo;
    const {city, province, telephoneCode, telephone} = sellerInfo.businessInfo;

    return (
        <div className='seller-info'>
            {profilePicUrl !== '' &&
            <div className='logo'>
                <img className='img' src={profilePicUrl} alt={fullName}/>
                <span className='score'>{shopRating.meanRate + '/' + shopRating.rateA}</span>
            </div>
            }
            <div className='details'>
                {businessName !== '' && <h1 className='title'>{businessName}</h1>}
                {city !== '' && <span className='location'>{`${city} / ${province}`}</span>}
                {biography && <p dangerouslySetInnerHTML={{__html: biography.replace(/\n/g, "<br/>")}}/>}
                {businessStatus === 1 &&
                <span className='verified'>اینی فای خرید شما را از این فروشگاه تضمین می نماید.</span>}
                {telephone !== '' &&
                <p>شماره پشتیبانی : <a href={`tel:${0 + telephoneCode + telephone}`}>{0 + telephoneCode + telephone}</a>
                </p>}
            </div>
            <img className='namad' src={namad}/>
        </div>
    )
};

export default Seller;
