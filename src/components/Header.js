import React from "react";
import {Link} from "react-router-dom";
import logo from '../assets/imgs/inify-logo.png';
import logo_typo from '../assets/imgs/inify-typo-logo.png';

const Header = () => (
    <header className='header'>
        <div className='logo-wrapper'>
            <Link to='/'>
                <img className='graphical-logo' src={logo} alt='Inify logo'/>
                <img className='typo-logo' src={logo_typo} alt='Inify typo logo'/>
            </Link>
        </div>
    </header>
);
export default Header;
