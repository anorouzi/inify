import React, {useEffect, useState} from "react";
import axios from "axios";

const Single = (props)=> {
    const [product, setProduct] = useState([]);
    useEffect(() => {
        axios.get(`ProductBriefDetails?id=${props.match.params.id}`)
            .then(res => {
                setProduct(res.data[0]);
            })
    }, []);

    return (
        <div className='single-product'>
            {product.imgUrl !== '' && <div className='img' style={{background:`url(${product.imgUrl})`}} />}
            <div className='detail'>
                {product.name !== '' && <h1 className='title'>{product.name}</h1>}
                {product.details !== undefined && product.details.map(item => (<div className='available-item'>{item.desc}<span className='price'>{item.price}ریال </span></div>))}
            </div>
        </div>
    )
};
export default Single;
