import React, {Fragment, useEffect, useState} from "react";
import Product from "./Product";
import axios from "axios";
import Seller from "../Seller";
const Archive = ()=> {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        axios.get(`ProductBriefDetails`)
            .then(res => {
                setProducts(res.data);
            })
    }, []);

    return (
        <Fragment>
            <Seller/>
            <div className='product-wrapper'>
                {products.map((item)=> <Product key={item.id} item={item}/>)}
            </div>
        </Fragment>
    )
};
export default Archive;
