import React from "react";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

const Product = (props) => {
    return (
        <Link to={`/product/${props.item.id}`}>
            <div className='product-item'>
                <div className='img' style={{background: `url(${props.item.imgUrl})`}}/>
                <div className='bottom'>
                    <h4 className='title'>{props.item.name}</h4>
                    <span className='price'>{props.item.minPrice} ریال </span>
                </div>
            </div>
        </Link>
    )
};
export default Product;

Product.propTypes = {
    item: PropTypes.object.isRequired
};
